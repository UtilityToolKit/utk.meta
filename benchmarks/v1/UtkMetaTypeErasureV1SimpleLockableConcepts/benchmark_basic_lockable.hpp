#ifndef BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_BENCHMARK_BASIC_LOCKABLE_HPP
#define BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_BENCHMARK_BASIC_LOCKABLE_HPP


#include "common.hpp"


nonius::benchmark BenchLockUnlockRawMutex () {
	return nonius::benchmark ("mutex", [](nonius::chronometer i_meter) {
		std::mutex mutex;

		i_meter.measure ([&]() {
			mutex.lock ();
			mutex.unlock ();
		});
	});
}


nonius::benchmark BenchLockUnlockUniqueLockMutex () {
	return nonius::benchmark ("unique_lock", [](nonius::chronometer i_meter) {
		std::mutex mutex;
		std::unique_lock< std::mutex > lock{mutex, std::defer_lock};

		i_meter.measure ([&]() {
			lock.lock ();
			lock.unlock ();
		});
	});
}


nonius::benchmark BenchLockUnlockAnyBasicLockableUniqueLockMutex () {
	return nonius::benchmark (
	    "AnyBasicLockable", [](nonius::chronometer i_meter) {
		    std::mutex mutex;

		    AnyBasicLockable any_basic_lockable{
		        std::unique_lock< std::mutex >{mutex, std::defer_lock}};

		    i_meter.measure ([&]() {
			    any_basic_lockable.lock ();
			    any_basic_lockable.unlock ();
		    });
	    });
}


nonius::benchmark BenchLockUnlockAnyLockableUniqueLockMutex () {
	return nonius::benchmark ("AnyLockable", [](nonius::chronometer i_meter) {
		std::mutex mutex;

		AnyLockable any_lockable{
		    std::unique_lock< std::mutex >{mutex, std::defer_lock}};

		i_meter.measure ([&]() {
			any_lockable.lock ();
			any_lockable.unlock ();
		});
	});
}


void BenchmarkBasicLockable () {
	nonius::configuration cfg;

	cfg.title = "utk::meta::type_erasure basic lockable concept";

	nonius::html_group_reporter reporter;
	reporter.set_output_file (
	    "benchmark_utk_meta_type_erasure_basic_lockable_concept.html");

	// lock()+unlock() benchmark group
	reporter.set_current_group_name (
	    "lock() + unlock()",
	    "Locking and unlocking <code>std::mutex</code>, "
	    "<code>std::unique_lock< std::mutex ></code>, <code>std::unique_lock< "
	    "std::mutex ></code> stored in AnyBasicLockable and AnyLockable.");

	nonius::benchmark benchmarks_lock_unlock[] = {
	    BenchLockUnlockRawMutex (),
	    BenchLockUnlockUniqueLockMutex (),
	    BenchLockUnlockAnyBasicLockableUniqueLockMutex (),
	    BenchLockUnlockAnyLockableUniqueLockMutex ()};

	nonius::go (
	    cfg,
	    std::begin (benchmarks_lock_unlock),
	    std::end (benchmarks_lock_unlock),
	    reporter);

	reporter.generate_report ();
}


#endif // BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_BENCHMARK_BASIC_LOCKABLE_HPP
