#ifndef BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_BENCHMARK_LOCKABLE_HPP
#define BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_BENCHMARK_LOCKABLE_HPP


#include "common.hpp"


nonius::benchmark BenchTryLockUnlockRawMutex () {
	return nonius::benchmark ("mutex", [](nonius::chronometer i_meter) {
		std::mutex mutex;

		i_meter.measure ([&]() {
			if (mutex.try_lock ()) {
				mutex.unlock ();
			}
		});
	});
}


nonius::benchmark BenchTryLockUnlockUniqueLockMutex () {
	return nonius::benchmark ("unique_lock", [](nonius::chronometer i_meter) {
		std::mutex mutex;
		std::unique_lock< std::mutex > lock{mutex, std::defer_lock};

		i_meter.measure ([&]() {
			if (lock.try_lock ()) {
				lock.unlock ();
			}
		});
	});
}


nonius::benchmark BenchTryLockUnlockAnyLockableUniqueLockMutex () {
	return nonius::benchmark ("AnyLockable", [](nonius::chronometer i_meter) {
		std::mutex mutex;

		AnyLockable any_lockable{
		    std::unique_lock< std::mutex >{mutex, std::defer_lock}};

		i_meter.measure ([&]() {
			if (any_lockable.try_lock ()) {
				any_lockable.unlock ();
			}
		});
	});
}


void BenchmarkLockable () {
	nonius::configuration cfg;

	cfg.title = "utk::meta::type_erasure lockable concept";

	nonius::html_group_reporter reporter;
	reporter.set_output_file (
	    "benchmark_utk_meta_type_erasure_lockable_concept.html");

	// try_lock() + unlock() benchmark group
	reporter.set_current_group_name (
	    "try_lock() + unlock()",
	    "Try-locking and unlocking <code>std::mutex</code>, "
	    "<code>std::unique_lock< std::mutex ></code>, <code>std::unique_lock< "
	    "std::mutex ></code> stored in AnyLockable.");

	nonius::benchmark benchmarks_try_lock_unlock[] = {
	    BenchTryLockUnlockRawMutex (),
	    BenchTryLockUnlockUniqueLockMutex (),
	    BenchTryLockUnlockAnyLockableUniqueLockMutex ()};

	nonius::go (
	    cfg,
	    std::begin (benchmarks_try_lock_unlock),
	    std::end (benchmarks_try_lock_unlock),
	    reporter);

	reporter.generate_report ();
}


#endif // BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_BENCHMARK_LOCKABLE_HPP
