#ifndef BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_COMMON_HPP
#define BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_COMMON_HPP


#include <nonius/html_group_reporter.h>
#include <nonius/nonius.h++>


#include <mutex>

#include <boost/mpl/assert.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/type_erasure/any.hpp>

#include <utk/meta/type_erasure/simple_lockable.hpp>


using AnyBasicLockable = ::boost::type_erasure::any< ::boost::mpl::vector<
    ::boost::type_erasure::constructible< ::boost::type_erasure::_self (
        ::boost::type_erasure::_self&&) >,
    ::boost::type_erasure::destructible<>,
    ::utk::meta::type_erasure::basic_lockable<>,
    ::boost::type_erasure::relaxed > >;


using AnyLockable = ::boost::type_erasure::any< ::boost::mpl::vector<
    ::boost::type_erasure::constructible< ::boost::type_erasure::_self (
        ::boost::type_erasure::_self&&) >,
    ::boost::type_erasure::destructible<>,
    ::utk::meta::type_erasure::lockable<>,
    ::boost::type_erasure::relaxed > >;


#endif // BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_COMMON_HPP
