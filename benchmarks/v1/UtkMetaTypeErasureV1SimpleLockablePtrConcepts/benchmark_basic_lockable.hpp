#ifndef BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTSPTR_BENCHMARK_BASIC_LOCKABLE_HPP
#define BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTSPTR_BENCHMARK_BASIC_LOCKABLE_HPP


#include "common.hpp"


nonius::benchmark BenchLockUnlockRawMutex () {
	return nonius::benchmark ("mutex", [](nonius::chronometer i_meter) {
		std::mutex mutex;

		i_meter.measure ([&]() {
			mutex.lock ();
			mutex.unlock ();
		});
	});
}


nonius::benchmark BenchLockUnlockSharedPtrMutex () {
	return nonius::benchmark ("shared_ptr", [](nonius::chronometer i_meter) {
		auto mutex_ptr = std::make_shared< std::mutex > ();

		i_meter.measure ([&]() {
			mutex_ptr->lock ();
			mutex_ptr->unlock ();
		});
	});
}


nonius::benchmark BenchLockUnlockAnyBasicLockablePtrWithoutUnrefToVarMutex () {
	return nonius::benchmark (
	    "AnyBasicLockablePtr (w/o unref)", [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};

		    auto any_basic_lockable = (*any_basic_lockable_ptr);

		    i_meter.measure ([&]() {
			    any_basic_lockable.lock ();
			    any_basic_lockable.unlock ();
		    });
	    });
}


nonius::benchmark BenchLockUnlockAnyBasicLockablePtrWithUnrefToVarMutex () {
	return nonius::benchmark (
	    "AnyBasicLockablePtr (unref to var)", [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};


		    i_meter.measure ([&]() {
			    auto any_basic_lockable = (*any_basic_lockable_ptr);

			    any_basic_lockable.lock ();
			    any_basic_lockable.unlock ();
		    });
	    });
}


nonius::benchmark BenchLockUnlockAnyBasicLockablePtrDirectUnrefMutex () {
	return nonius::benchmark (
	    "AnyBasicLockablePtr (direct unref)", [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};


		    i_meter.measure ([&]() {
			    (*any_basic_lockable_ptr).lock ();
			    (*any_basic_lockable_ptr).unlock ();
		    });
	    });
}


nonius::benchmark
    BenchLockUnlockAnyBasicLockablePtrUnrefToVarUniqueLockRAIIMutex () {
	return nonius::benchmark (
	    "AnyBasicLockablePtr (RAII unique_lock)",
	    [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};

		    auto any_basic_lockable = (*any_basic_lockable_ptr);

		    /*
		      I hope this will somehow protect the lambda body from being
		      optimised out.
		    */
		    volatile int a = 5;

		    i_meter.measure ([&]() {
			    std::unique_lock< decltype (any_basic_lockable) > unique_lock{
			        any_basic_lockable};

			    a = 8;
		    });

		    a = 10;
	    });
}


nonius::benchmark
    BenchLockUnlockAnyBasicLockablePtrUnrefToVarUniqueLockMutex () {
	return nonius::benchmark (
	    "AnyBasicLockablePtr (defer unique_lock)",
	    [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};

		    auto any_basic_lockable = (*any_basic_lockable_ptr);

		    std::unique_lock< decltype (any_basic_lockable) > unique_lock{
		        any_basic_lockable, std::defer_lock};

		    i_meter.measure ([&]() {
			    unique_lock.lock ();
			    unique_lock.unlock ();
		    });
	    });
}


void BenchmarkBasicLockable () {
	nonius::configuration cfg;

	cfg.title = "utk::meta::type_erasure basic lockable concept";

	nonius::html_group_reporter reporter;
	reporter.set_output_file (
	    "benchmark_utk_meta_type_erasure_basic_lockable_concept.html");

	// lock()+unlock() benchmark group
	reporter.set_current_group_name (
	    "lock() + unlock()",
	    "Locking and unlocking <code>std::mutex</code> "
	    "stored in AnyBasicLockablePtr and AnyLockablePtr with different "
	    "access patterns.");

	nonius::benchmark benchmarks_lock_unlock[] = {
	    BenchLockUnlockRawMutex (),
	    BenchLockUnlockSharedPtrMutex (),
	    BenchLockUnlockAnyBasicLockablePtrWithoutUnrefToVarMutex (),
	    BenchLockUnlockAnyBasicLockablePtrWithUnrefToVarMutex (),
	    BenchLockUnlockAnyBasicLockablePtrDirectUnrefMutex (),
	    BenchLockUnlockAnyBasicLockablePtrUnrefToVarUniqueLockRAIIMutex (),
	    BenchLockUnlockAnyBasicLockablePtrUnrefToVarUniqueLockMutex ()};

	nonius::go (
	    cfg,
	    std::begin (benchmarks_lock_unlock),
	    std::end (benchmarks_lock_unlock),
	    reporter);

	reporter.generate_report ();
}


#endif // BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTSPTR_BENCHMARK_BASIC_LOCKABLE_HPP
