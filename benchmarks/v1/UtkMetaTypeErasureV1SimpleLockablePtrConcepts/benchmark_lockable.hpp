#ifndef BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLEPTRCONCEPTS_BENCHMARK_LOCKABLE_HPP
#define BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLEPTRCONCEPTS_BENCHMARK_LOCKABLE_HPP


#include "common.hpp"


nonius::benchmark BenchTryLockUnlockRawMutex () {
	return nonius::benchmark ("mutex", [](nonius::chronometer i_meter) {
		std::mutex mutex;

		i_meter.measure ([&]() {
			if (mutex.try_lock ()) {
				mutex.unlock ();
			}
		});
	});
}


nonius::benchmark BenchTryLockUnlockSharedPtrMutex () {
	return nonius::benchmark ("shared_ptr", [](nonius::chronometer i_meter) {
		auto mutex_ptr = std::make_shared< std::mutex > ();

		i_meter.measure ([&]() {
			if (mutex_ptr->try_lock ()) {
				mutex_ptr->unlock ();
			}
		});
	});
}


nonius::benchmark BenchTryLockUnlockAnyLockablePtrWithoutUnrefToVarMutex () {
	return nonius::benchmark (
	    "AnyLockablePtr (w/o unref)", [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};

		    auto any_basic_lockable = (*any_basic_lockable_ptr);

		    i_meter.measure ([&]() {
			    if (any_basic_lockable.try_lock ()) {
				    any_basic_lockable.unlock ();
			    }
		    });
	    });
}


nonius::benchmark BenchTryLockUnlockAnyLockablePtrWithUnrefToVarMutex () {
	return nonius::benchmark (
	    "AnyLockablePtr (unref to var)", [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};


		    i_meter.measure ([&]() {
			    auto any_basic_lockable = (*any_basic_lockable_ptr);

			    if (any_basic_lockable.try_lock ()) {
				    any_basic_lockable.unlock ();
			    }
		    });
	    });
}


nonius::benchmark BenchTryLockUnlockAnyLockablePtrDirectUnrefMutex () {
	return nonius::benchmark (
	    "AnyLockablePtr (direct unref)", [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};


		    i_meter.measure ([&]() {
			    if ((*any_basic_lockable_ptr).try_lock ()) {
				    (*any_basic_lockable_ptr).unlock ();
			    }
		    });
	    });
}


nonius::benchmark BenchTryLockUnlockAnyLockablePtrUnrefToVarUniqueLockMutex () {
	return nonius::benchmark (
	    "AnyLockablePtr (defer unique_lock)", [](nonius::chronometer i_meter) {
		    AnyLockablePtr any_basic_lockable_ptr{
		        std::make_shared< std::mutex > ()};

		    auto any_basic_lockable = (*any_basic_lockable_ptr);

		    std::unique_lock< decltype (any_basic_lockable) > unique_lock{
		        any_basic_lockable, std::defer_lock};

		    i_meter.measure ([&]() {
			    if (unique_lock.try_lock ()) {
				    unique_lock.unlock ();
			    }
		    });
	    });
}


void BenchmarkLockable () {
	nonius::configuration cfg;

	cfg.title = "utk::meta::type_erasure lockable concept";

	nonius::html_group_reporter reporter;
	reporter.set_output_file (
	    "benchmark_utk_meta_type_erasure_lockable_concept.html");

	// try_lock() + unlock() benchmark group
	reporter.set_current_group_name (
	    "try_lock() + unlock()",
	    "Try-locking and unlocking <code>std::mutex</code> "
	    "stored in AnyLockablePtr and AnyLockablePtr with different "
	    "access patterns.");

	nonius::benchmark benchmarks_try_lock_unlock[] = {
	    BenchTryLockUnlockRawMutex (),
	    BenchTryLockUnlockSharedPtrMutex (),
	    BenchTryLockUnlockAnyLockablePtrWithoutUnrefToVarMutex (),
	    BenchTryLockUnlockAnyLockablePtrWithUnrefToVarMutex (),
	    BenchTryLockUnlockAnyLockablePtrDirectUnrefMutex (),
	    BenchTryLockUnlockAnyLockablePtrUnrefToVarUniqueLockMutex ()};

	nonius::go (
	    cfg,
	    std::begin (benchmarks_try_lock_unlock),
	    std::end (benchmarks_try_lock_unlock),
	    reporter);

	reporter.generate_report ();
}


#endif // BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLEPTRCONCEPTS_BENCHMARK_LOCKABLE_HPP
