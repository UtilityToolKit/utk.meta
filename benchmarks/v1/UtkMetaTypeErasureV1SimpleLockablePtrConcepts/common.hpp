#ifndef BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_COMMON_HPP
#define BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_COMMON_HPP


#include <nonius/html_group_reporter.h>
#include <nonius/nonius.h++>


#include <mutex>
#include <memory>

#include <boost/mpl/vector.hpp>
#include <boost/type_erasure/any.hpp>

#include <utk/meta/type_erasure/simple_lockable.hpp>
#include <utk/meta/type_erasure/simple_lockable_ptr.hpp>


using AnyBasicLockablePtr = ::boost::type_erasure::any<
    ::boost::mpl::vector< ::utk::meta::type_erasure::basic_lockable_ptr > >;
using AnyBasicLockableWeakPtr =
    ::boost::type_erasure::any< ::boost::mpl::vector<
        ::utk::meta::type_erasure::basic_lockable_weak_ptr > >;


using AnyLockablePtr = ::boost::type_erasure::any<
    ::boost::mpl::vector< ::utk::meta::type_erasure::lockable_ptr > >;
using AnyLockableWeakPtr = ::boost::type_erasure::any<
    ::boost::mpl::vector< ::utk::meta::type_erasure::lockable_weak_ptr > >;


#endif // BENCHMARKS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_COMMON_HPP
