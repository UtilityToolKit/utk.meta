#ifndef UNIT_TESTS_V1_UTKMETATYPEERASUREMEMBERMACRO_TEST_HPP
#define UNIT_TESTS_V1_UTKMETATYPEERASUREMEMBERMACRO_TEST_HPP


#include <gtest/gtest.h>


#include <boost/type_erasure/any.hpp>

#include <utk/meta/type_erasure/detail/member.hpp>


UTK_META_TYPE_ERASURE_MEMBER (get)


template < class T >
using AnyGettable = boost::type_erasure::any< boost::mpl::vector<
    boost::type_erasure::constructible< boost::type_erasure::_self () >,
    boost::type_erasure::destructible<>,
    utk::meta::type_erasure::has_get< T*(), boost::type_erasure::_self > > >;


struct Gettable {
	int* get ();
};


struct NotGettable {
	void not_get ();
};


TEST (TestUtkMetaTypeErasureMemberMacro, CompilationTest) {
	AnyGettable< int > var{Gettable{}};

	// AnyGettable< int > bad_var{NotGettable{}}; // Compilation error
}


#endif /* UNIT_TESTS_V1_UTKMETATYPEERASUREMEMBERMACRO_TEST_HPP */
