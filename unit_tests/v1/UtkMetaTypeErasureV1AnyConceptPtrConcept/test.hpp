#ifndef UNIT_TESTS_V1_UTKMETATYPEERASUREV1ANYCONCEPTPTRCONCEPT_TEST_HPP
#define UNIT_TESTS_V1_UTKMETATYPEERASUREV1ANYCONCEPTPTRCONCEPT_TEST_HPP


#include <gtest/gtest.h>


#include <memory>

#include <boost/mpl/vector.hpp>

#include <boost/type_erasure/any.hpp>
#include <boost/type_erasure/exception.hpp>

#include <utk/meta/type_erasure/any_concept_ptr.hpp>


BOOST_TYPE_ERASURE_MEMBER ((has_get), get)


struct Gettable {
	static constexpr int kValue = 3;

	const int value = kValue;

	int get () {
		return value;
	}
};


template < class T = ::boost::type_erasure::_self >
struct gettable_intermediate : ::boost::mpl::vector< has_get< int(), T > > {};


template < class T = ::boost::type_erasure::_self >
using gettable_alias = has_get< int(), T >;


TEST (UtkMetaTypeErasureV1AnyConceptPtrConcept, IntermediateConceptTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR (gettable_ptr, gettable_intermediate)

	using any_gettable_ptr = ::boost::type_erasure::any< gettable_ptr >;

	any_gettable_ptr ptr{std::shared_ptr< Gettable >{new Gettable{}}};

	ASSERT_EQ ((*ptr).get (), Gettable::kValue);

	gettable_ptr::element_type ref (*ptr);

	ASSERT_EQ (ref.get (), Gettable::kValue);
}


TEST (UtkMetaTypeErasureV1AnyConceptPtrConcept, AliasConceptTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR (gettable_ptr, gettable_alias)

	using any_gettable_ptr = ::boost::type_erasure::any< gettable_ptr >;

	any_gettable_ptr ptr{std::shared_ptr< Gettable >{new Gettable{}}};

	ASSERT_EQ ((*ptr).get (), Gettable::kValue);

	gettable_ptr::element_type ref (*ptr);

	ASSERT_EQ (ref.get (), Gettable::kValue);
}


TEST (UtkMetaTypeErasureV1AnyConceptPtrConcept, CastToBoolTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR (gettable_ptr, gettable_alias)

	using any_gettable_ptr = ::boost::type_erasure::any< gettable_ptr >;

	any_gettable_ptr ptr{};

	bool is_empty{false};

	// Test default constructed object
	try {
		volatile bool is_null = bool(ptr);
	}
	catch (::boost::type_erasure::bad_function_call& i_error) {
		is_empty = true;
	}

	EXPECT_TRUE (is_empty)
	    << "Default constructed any_concept_ptr must be invalid";

	// Test object containing nullptr
	ptr = std::shared_ptr< Gettable >{};
	is_empty = false;

	bool is_null{false};

	try {
		is_null = bool(ptr);
	}
	catch (::boost::type_erasure::bad_function_call& i_error) {
		is_empty = true;
	}

	EXPECT_FALSE (is_empty)
	    << "After setting its value any_concept_ptr must be valid";

	EXPECT_FALSE (is_null)
	    << "any_concept_ptr with nullptr inside must cast to false";

	// Test object containing valid pointer
	ptr = std::shared_ptr< Gettable >{new Gettable{}};
	is_empty = false;

	try {
		is_null = bool(ptr);
	}
	catch (::boost::type_erasure::bad_function_call& i_error) {
		is_empty = true;
	}

	EXPECT_FALSE (is_empty)
	    << "After setting its value any_concept_ptr must be valid";

	EXPECT_TRUE (is_null)
	    << "any_concept_ptr with valid pointer inside must cast to true";
}


#endif /* UNIT_TESTS_V1_UTKMETATYPEERASUREV1ANYCONCEPTPTRCONCEPT_TEST_HPP */
