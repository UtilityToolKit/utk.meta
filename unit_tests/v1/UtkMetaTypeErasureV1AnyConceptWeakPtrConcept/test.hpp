#ifndef UNIT_TESTS_V1_UTKMETATYPEERASUREV1ANYCONCEPTWEAKPTRCONCEPT_TEST_HPP
#define UNIT_TESTS_V1_UTKMETATYPEERASUREV1ANYCONCEPTWEAKPTRCONCEPT_TEST_HPP


#include <gtest/gtest.h>


#include <memory>

#include <boost/mpl/vector.hpp>

#include <boost/type_erasure/any.hpp>
#include <boost/type_erasure/exception.hpp>

#include <utk/meta/type_erasure/any_concept_ptr.hpp>
#include <utk/meta/type_erasure/any_concept_weak_ptr.hpp>


BOOST_TYPE_ERASURE_MEMBER ((has_get), get)


struct Gettable {
	static constexpr int kValue = 3;

	const int value = kValue;

	int get () {
		return value;
	}
};


template < class T = ::boost::type_erasure::_self >
struct gettable_intermediate : ::boost::mpl::vector< has_get< int(), T > > {};


template < class T = ::boost::type_erasure::_self >
using gettable_alias = has_get< int(), T >;


TEST (UtkMetaTypeErasureV1AnyConceptWeakPtrConcept, IntermediateConceptTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR (gettable_ptr, gettable_intermediate)
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (
	    gettable_weak_ptr, gettable_intermediate)

	using any_gettable_ptr = ::boost::type_erasure::any< gettable_ptr >;

	using any_gettable_weak_ptr =
	    ::boost::type_erasure::any< gettable_weak_ptr >;

	auto shared_ptr = std::shared_ptr< Gettable >{new Gettable{}};

	any_gettable_ptr ptr{shared_ptr};

	any_gettable_weak_ptr weak_ptr{std::weak_ptr< Gettable >{shared_ptr}};

	ASSERT_EQ ((*weak_ptr.lock ()).get (), Gettable::kValue);

	gettable_weak_ptr::element_type ref (*weak_ptr.lock ());

	ASSERT_EQ (ref.get (), Gettable::kValue);
}


TEST (UtkMetaTypeErasureV1AnyConceptWeakPtrConcept, AliasConceptTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR (gettable_ptr, gettable_alias)
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (
	    gettable_weak_ptr, gettable_alias)

	using any_gettable_ptr = ::boost::type_erasure::any< gettable_ptr >;

	using any_gettable_weak_ptr =
	    ::boost::type_erasure::any< gettable_weak_ptr >;

	auto shared_ptr = std::shared_ptr< Gettable >{new Gettable{}};

	any_gettable_ptr ptr{shared_ptr};

	any_gettable_weak_ptr weak_ptr{std::weak_ptr< Gettable >{shared_ptr}};

	ASSERT_EQ ((*weak_ptr.lock ()).get (), Gettable::kValue);

	gettable_weak_ptr::element_type ref (*weak_ptr.lock ());

	ASSERT_EQ (ref.get (), Gettable::kValue);
}


TEST (UtkMetaTypeErasureV1AnyConceptWeakPtrConcept, EmptyValueTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (
	    gettable_weak_ptr, gettable_alias)

	using any_gettable_weak_ptr =
	    ::boost::type_erasure::any< gettable_weak_ptr >;

	any_gettable_weak_ptr weak_ptr{};

	bool is_empty{false};

	try {
		volatile bool expired = weak_ptr.expired ();

		(void)expired;
	}
	catch (::boost::type_erasure::bad_function_call& i_error) {
		is_empty = true;
	}

	EXPECT_TRUE (is_empty)
	    << "Default constructed any_concept_weak_ptr must be invalid";
}


TEST (UtkMetaTypeErasureV1AnyConceptWeakPtrConcept, ExpiredTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (
	    gettable_weak_ptr, gettable_alias)

	using any_gettable_weak_ptr =
	    ::boost::type_erasure::any< gettable_weak_ptr >;


	auto ptr = std::shared_ptr< Gettable >{};

	any_gettable_weak_ptr weak_ptr{std::weak_ptr< Gettable >{ptr}};

	bool is_empty = false;

	bool expired{false};

	try {
		expired = weak_ptr.expired ();
	}
	catch (::boost::type_erasure::bad_function_call& i_error) {
		is_empty = true;
	}

	EXPECT_FALSE (is_empty)
	    << "After setting its value any_concept_weak_ptr must be valid";

	EXPECT_TRUE (expired)
	    << "any_concept_weak_ptr with nullptr inside must be expired";
}


TEST (UtkMetaTypeErasureV1AnyConceptWeakPtrConcept, NonExpiredTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (
	    gettable_weak_ptr, gettable_alias)

	using any_gettable_weak_ptr =
	    ::boost::type_erasure::any< gettable_weak_ptr >;


	auto ptr = std::make_shared< Gettable > ();

	any_gettable_weak_ptr weak_ptr{std::weak_ptr< Gettable >{ptr}};

	bool is_empty = false;

	bool expired{false};

	try {
		expired = weak_ptr.expired ();
	}
	catch (::boost::type_erasure::bad_function_call& i_error) {
		is_empty = true;
	}

	EXPECT_FALSE (is_empty)
	    << "After setting its value any_concept_weak_ptr must be valid";

	EXPECT_FALSE (expired) << "any_concept_weak_ptr with a valid pointer "
	                          "inside must be not expired";
}


TEST (UtkMetaTypeErasureV1AnyConceptWeakPtrConcept, LockAndUseCountTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (
	    gettable_weak_ptr, gettable_alias)

	using any_gettable_weak_ptr =
	    ::boost::type_erasure::any< gettable_weak_ptr >;

	auto ptr = std::make_shared< Gettable > ();

	any_gettable_weak_ptr weak_ptr{std::weak_ptr< Gettable >{ptr}};

	EXPECT_EQ (ptr.use_count (), weak_ptr.use_count ()) << "Before lock";

	{
		auto ptr_lock = weak_ptr.lock ();

		EXPECT_EQ (ptr.use_count (), weak_ptr.use_count ()) << "After lock";
	}

	EXPECT_EQ (ptr.use_count (), weak_ptr.use_count ()) << "After unlock";
}


TEST (UtkMetaTypeErasureV1AnyConceptWeakPtrConcept, ResetTest) {
	UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (
	    gettable_weak_ptr, gettable_alias)

	using any_gettable_weak_ptr =
	    ::boost::type_erasure::any< gettable_weak_ptr >;

	auto ptr = std::make_shared< Gettable > ();

	any_gettable_weak_ptr weak_ptr{std::weak_ptr< Gettable >{ptr}};

	EXPECT_EQ (ptr.use_count (), weak_ptr.use_count ())
	    << "After initialisation";

	weak_ptr.reset ();

	EXPECT_TRUE (weak_ptr.expired ()) << "After reset";
}


#endif // UNIT_TESTS_V1_UTKMETATYPEERASUREV1ANYCONCEPTWEAKPTRCONCEPT_TEST_HPP
