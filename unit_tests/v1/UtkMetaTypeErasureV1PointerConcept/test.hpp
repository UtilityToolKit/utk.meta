#ifndef UNIT_TESTS_V1_UTKMETATYPEERASUREV1POINTERCONCEPT_TEST_HPP
#define UNIT_TESTS_V1_UTKMETATYPEERASUREV1POINTERCONCEPT_TEST_HPP


#include <gtest/gtest.h>


#include <mutex>
#include <sstream>

#include <boost/mpl/assert.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/type_erasure/any.hpp>
#include <boost/type_erasure/any_cast.hpp>

#include <utk/meta/type_erasure/pointer.hpp>


using AnyPointer = ::boost::type_erasure::any< ::boost::mpl::vector<
    ::boost::type_erasure::
        ostreamable< std::ostream, ::boost::type_erasure::_a >,
    ::boost::type_erasure::same_type<
        ::utk::meta::type_erasure::pointer<>::element_type,
        ::boost::type_erasure::_a >,
    ::boost::type_erasure::typeid_<
        ::utk::meta::type_erasure::pointer<>::element_type >,
    ::utk::meta::type_erasure::pointer<> > >;


TEST (TestUtkMetaTypeErasureV1PointerConcept, PointerConteptTest) {
	int var = 10;

	AnyPointer any_var{&var};

	ASSERT_EQ (::boost::type_erasure::any_cast< int > (*any_var), var)
	    << "The dereferenced value must be equal to the referenced variable "
	       "value.";
}


#endif /* UNIT_TESTS_V1_UTKMETATYPEERASUREV1POINTERCONCEPT_TEST_HPP */
