#ifndef UNIT_TESTS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_TEST_HPP
#define UNIT_TESTS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_TEST_HPP


#include <gtest/gtest.h>


#include <mutex>

#include <boost/mpl/assert.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/type_erasure/any.hpp>

#include <utk/meta/type_erasure/simple_lockable.hpp>


using AnyBasicLockable = ::boost::type_erasure::any< ::boost::mpl::vector<
    ::boost::type_erasure::constructible< ::boost::type_erasure::_self (
        ::boost::type_erasure::_self&&) >,
    ::boost::type_erasure::destructible<>,
    ::utk::meta::type_erasure::basic_lockable<>,
    ::boost::type_erasure::relaxed > >;


TEST (
    TestUtkMetaTypeErasureV1SimpleLockableConcepts,
    BasicLockableTypeErasureConteptTest) {
	std::mutex mutex{};

	/*
	  Using std::unique_lock to initialise the AnyBasicLockable because it is
	  movable, unlike the std::mutex.
	*/
	AnyBasicLockable any_basic_lockable{
	    std::unique_lock< std::mutex >{mutex, std::defer_lock}};

	// Check that the mutex is not locked.
	ASSERT_TRUE (mutex.try_lock ())
	    << "The mutex must be unlocked. Possible error in test.";

	mutex.unlock ();

	// Check that locking an AnyBasicLockable object locks the underlying
	// lockable.
	any_basic_lockable.lock ();

	ASSERT_FALSE (mutex.try_lock ())
	    << "The mutex must be unlocked after the AnyBasicLockable object "
	       "lock() member function call.";

	// Check that unlocking an AnyBasicLockable object unlocks the underlying
	// lockable.
	any_basic_lockable.unlock ();

	ASSERT_TRUE (mutex.try_lock ())
	    << "The mutex must be unlocked after the AnyBasicLockable object "
	       "unlock() member function call.";

	mutex.unlock ();
}


using AnyLockable = ::boost::type_erasure::any< ::boost::mpl::vector<
    ::boost::type_erasure::constructible< ::boost::type_erasure::_self (
        ::boost::type_erasure::_self&&) >,
    ::boost::type_erasure::destructible<>,
    ::utk::meta::type_erasure::lockable<>,
    ::boost::type_erasure::relaxed > >;


TEST (
    TestUtkMetaTypeErasureV1SimpleLockableConcepts,
    LockableTypeErasureConteptTest) {
	std::mutex mutex{};

	/*
	  Using std::unique_lock to initialise the AnyBasicLockable because it is
	  movable, unlike the std::mutex.
	*/
	AnyLockable any_lockable{
	    std::unique_lock< std::mutex >{mutex, std::defer_lock}};

	// Check that the mutex is not locked.
	ASSERT_TRUE (mutex.try_lock ())
	    << "The mutex must be unlocked. Possible error in test.";

	mutex.unlock ();

	// Check that locking an AnyBasicLockable object locks the underlying
	// lockable.
	any_lockable.lock ();

	ASSERT_FALSE (mutex.try_lock ())
	    << "The mutex must be locked after the AnyBasicLockable object "
	       "lock() member function call.";

	// Check that unlocking an AnyBasicLockable object unlocks the underlying
	// lockable.
	any_lockable.unlock ();

	ASSERT_TRUE (mutex.try_lock ())
	    << "The mutex must be unlocked after the AnyBasicLockable object "
	       "unlock() member function call.";

	mutex.unlock ();

	// Check that try-locking of an AnyBasicLockable object unlocks the
	// underlying lockable.
	ASSERT_TRUE (any_lockable.try_lock ())
	    << "The AnyBasicLockable object try_lock() member function call must "
	       "succeed with the unlocked underlying mutex.";

	ASSERT_FALSE (mutex.try_lock ()) << "The mutex must be locked. The "
	                                    "AnyBasicLockable class does not work.";

	{
		bool already_locked{false};

		try {
			any_lockable.try_lock ();
		}
		catch (std::system_error& i_error) {
			already_locked =
			    (i_error.code () == std::errc::resource_deadlock_would_occur);
		}

		ASSERT_TRUE (already_locked)
		    << "The AnyBasicLockable object try_lock() member function call "
		       "must not succeed with the locked underlying mutex.";
	}

	mutex.unlock ();
}


#endif /* UNIT_TESTS_V1_UTKMETATYPEERASUREV1SIMPLELOCKABLECONCEPTS_TEST_HPP */
