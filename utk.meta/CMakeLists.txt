# Copyright 2018 Utility Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy of
# the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#
# File name: utk.meta/CMakeLists.txt
#
# Description: utk.meta library project definition.

cmake_minimum_required (VERSION 3.3 FATAL_ERROR)


include (dependencies.cmake)


project (utk_meta
  LANGUAGES CXX
  VERSION   0.1.0
  )

######################################
# Project target and other variables #
######################################
set (${PROJECT_NAME}_TARGET_NAME ${PROJECT_NAME})
set (${PROJECT_NAME}_TARGET_EXPORT_NAME "utk::meta")


#####################
# Find dependencies #
#####################
utk_meta_find_dependencies ()


################################################
# Create project target and set its properties #
################################################
add_library (${${PROJECT_NAME}_TARGET_NAME} INTERFACE)
# Alias target is required to use as subproject
add_library (
  ${${PROJECT_NAME}_TARGET_EXPORT_NAME} ALIAS ${${PROJECT_NAME}_TARGET_NAME})

# Using target list for unification
set (${PROJECT_NAME}_TARGET_LIST
  "${${PROJECT_NAME}_TARGET_NAME}"
  )

# Export options
set_target_properties(${${PROJECT_NAME}_TARGET_NAME}
  PROPERTIES
  EXPORT_NAME "${${PROJECT_NAME}_TARGET_EXPORT_NAME}")

# Versioning properties
set_target_properties (
  ${${PROJECT_NAME}_TARGET_NAME}
  PROPERTIES
  COMPATIBLE_INTERFACE_STRING  "${PROJECT_NAME}_MAJOR_VERSION"
  )

# utk.cmake specific options
set_target_properties (
  ${${PROJECT_NAME}_TARGET_NAME}
  PROPERTIES
  INTERFACE_UTK_CMAKE_INCLUDE_PREFIX         "utk/meta"
  INTERFACE_UTK_CMAKE_LANGUAGE               "CXX"
  INTERFACE_UTK_CMAKE_PROJECT_CXX_NAMESPACE  "utk;meta;inline;v1"
  )


####################
# Use dependencies #
####################
utk_meta_use_dependencies (TARGET ${${PROJECT_NAME}_TARGET_LIST})


#################
# Export header #
#################
utk_cmake_generate_export_header (
  TARGET ${${PROJECT_NAME}_TARGET_NAME}
  COMMON_BASE_NAME "${PROJECT_NAME}"
  )


##########################
# Versioning information #
##########################
utk_cmake_product_information (
  TARGET                    ${${PROJECT_NAME}_TARGET_NAME}
  COMMON_TARGET_IDENTIFIER  "${PROJECT_NAME}"
  CREATE_PRODUCT_INFO_FUNCTIONS
  NAME_STRING               "Utility Toolkit Meta Support Library"
  CONTRIBUTORS_FILE         "${CMAKE_CURRENT_LIST_DIR}/../CONTRIBUTORS"
  COPYRIGHT_HOLDER_STRING   "Utility Tool Kit Open Source contributors"
  COPYRIGHT_INFO_FILE       "product_info/COPYRIGHT_INFO"
  COPYRIGHT_INFO_UTF8_FILE  "product_info/COPYRIGHT_INFO"
  LICENSE_FILE              "${CMAKE_CURRENT_LIST_DIR}/../LICENSE"
  LICENSE_NAME_STRING       "Apache License 2.0"
#  NOTICE_FILE               "NOTICE"  # There is no NOTICE file right now
  )


##########################################
# Include directories and link libraries #
##########################################
# The only place where the INTERFACE_INCLUDE_DIRECTORIES property is set. The
# utk_cmake_install does not do this because it does not provide a way to set
# properties for INTERFACE_LIBRARY targets.
utk_cmake_target_include_directories(
  TARGET  ${${PROJECT_NAME}_TARGET_LIST}
  INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
  INTERFACE
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
  )


#########################################################
# Subdirectories with headers, sources, resources, etc. #
#########################################################
add_subdirectory (include)
add_subdirectory (src)


##########################################
# Install and export project and targets #
##########################################
include (GNUInstallDirs)

utk_cmake_install_project (
  TARGET           ${${PROJECT_NAME}_TARGET_NAME}
  INSTALL_DEVEL    ${${PROJECT_NAME}_INSTALL_DEVEL}
  INSTALL_SOURCES  ${${PROJECT_NAME}_INSTALL_DEVEL}

  CUSTOM_CMAKE_CONFIG_FILE  "${CMAKE_CURRENT_LIST_DIR}/cmake/cmake-target-config.cmake.in"

  CMAKE_CONFIG_FILE_OPTIONS
  INSTALL_DESTINATION  "${CMAKE_INSTALL_LIBDIR}/cmake/@TARGET_NAME@"

  CMAKE_CONFIG_VERSION_FILE_OPTIONS
  COMPATIBILITY  "ExactVersion" # For initial development, change to
                                # "SameMajorVersion" after the interface is
                                # stable.

  CMAKE_PACKAGE_FILE_OPTIONS
  COMPONENT    "Devel"
  DESTINATION  "${CMAKE_INSTALL_LIBDIR}/cmake/@TARGET_NAME@"
  )
