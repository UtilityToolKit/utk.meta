# Copyright 2018 Utility Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy of
# the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#
# File name: utk.meta/CMakeLists.txt
#
# Description: utk.meta library dependencies.


#####################
# Find dependencies #
#####################
function (utk_meta_find_dependencies)
  set (_options
    USE_FIND_DEPENDENCIES
    )
  set (_multi_value_args
    ""
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})


  if (i_USE_FIND_DEPENDENCIES)
    macro (find_function)
      include (CMakeFindDependencyMacro)

      find_dependency (${ARGN})
    endmacro (find_function)
  else ()
    macro (find_function)
      find_package (${ARGN})
    endmacro (find_function)
  endif ()

  find_package (Boost 1.67 REQUIRED)
endfunction (utk_meta_find_dependencies)


####################
# Use dependencies #
####################
function (utk_meta_use_dependencies)
  set (_options
    )
  set (_multi_value_args
    TARGET
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})

  if (NOT i_TARGET)
    message (SEND_ERROR "Provide IMPORTED_TARGET or INTERFACE_TARGET argument")
  endif ()

  foreach (_target IN LISTS i_TARGET)
    target_link_libraries (
      ${_target}
      INTERFACE
      Boost::boost
      )
  endforeach (_target IN LISTS _interface_targets)
endfunction (utk_meta_use_dependencies)
