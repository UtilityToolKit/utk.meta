// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: include/utk/idiom/has_member_function.hpp
//
// Description: Provides marcos for checking if the type has member function
//              with the given signature. The implementation is based on
//              https://stackoverflow.com/a/16824239/1292814


#ifndef INCLUDE_UTK_IDIOM_HAS_MEMBER_FUNCTION_HPP
#define INCLUDE_UTK_IDIOM_HAS_MEMBER_FUNCTION_HPP


#define GENERIC_HAS_NON_TEMPLATE_MEMBER_FUNCTION(FUNCTION_NAME)                    \
	template < typename, typename T >                                              \
	struct generic_has_##FUNCTION_NAME {                                           \
		static_assert (                                                            \
		    std::integral_constant< T, false >::value,                             \
		    "Second template parameter needs to be of function type.");            \
	};                                                                             \
                                                                                   \
                                                                                   \
	template < typename Class, typename ReturnType, typename... Args >             \
	struct generic_has_##FUNCTION_NAME< Class, ReturnType (Args...) > {            \
	private:                                                                       \
		template < typename T >                                                    \
		static constexpr auto check (T*) -> typename std::is_same<                 \
		    decltype (std::declval< T > ().FUNCTION_NAME (                         \
		        std::declval< Args > ()...)),                                      \
		    ReturnType /* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/ \
		    >::type; /* attempt to call it and see if the return type is           \
		                correct */                                                 \
                                                                                   \
		template < typename >                                                      \
		static constexpr std::false_type check (...);                              \
                                                                                   \
		typedef decltype (check< Class > (nullptr)) type;                          \
                                                                                   \
                                                                                   \
	public:                                                                        \
		static constexpr bool value = type::value;                                 \
	};


#define SPECIFIC_HAS_NON_TEMPLATE_MEMBER_FUNCTION_CUSTOM_POSTFIX(              \
    FUNCTION_NAME, TRAIT_POSTFIX, ...)                                         \
	template < typename Class >                                                \
	using has_##FUNCTION_NAME##TRAIT_POSTFIX =                                 \
	    generic_has_##FUNCTION_NAME< Class, __VA_ARGS__ >;


#define SPECIFIC_HAS_NON_TEMPLATE_MEMBER_FUNCTION_NO_POSTFIX(                  \
    FUNCTION_NAME, ...)                                                        \
	SPECIFIC_HAS_NON_TEMPLATE_MEMBER_FUNCTION_CUSTOM_POSTFIX (                 \
	    FUNCTION_NAME, , __VA_ARGS__)


#define SPECIFIC_HAS_NON_TEMPLATE_MEMBER_FUNCTION(                             \
    FUNCTION_NAME, TRAIT_POSTFIX, ...)                                         \
	SPECIFIC_HAS_NON_TEMPLATE_MEMBER_FUNCTION_CUSTOM_POSTFIX (                 \
	    FUNCTION_NAME, _##TRAIT_POSTFIX, __VA_ARGS__)


#endif /* INCLUDE_UTK_IDIOM_HAS_MEMBER_FUNCTION_HPP */
