// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.meta/utk.meta/include/utk/meta/type_erasure/any_concept_ptr.hpp
//
// Description: Declaration of a type erasure concept for a pointer to an object
//              of a type implementing the given concepts for using with
//              "boost::type_erasure::any".


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR_HPP


#include <type_traits>

#include <boost/mpl/vector.hpp>

#include <boost/type_erasure/any.hpp>

#include "utk/meta/type_erasure/detail/namespace.hpp"

#include "utk/meta/type_erasure/pointer.hpp"
#include "utk/meta/type_erasure/type_cast_operator.hpp"


UTK_META_TYPE_ERASURE_EXPLICITLY_CONVERTIBLE_TO_QUALIFIED (
    bool, UTK_META_TYPE_ERASURE_PRIVATE_VERSIONED_NAMESPACE_SEQUENCE)


UTK_META_TYPE_ERASURE_V1_BEGIN_NAMESPACE


/**
   @brief A type erasure concept for a pointer to an object of a type
   implementing the given concepts

   @details The concepts given in ConceptHead and ConceptTail template arguments
   are applied to the type referenced by the pointer. The concepts in these
   arguments should take one template argument that defines the type they are
   applied to. In case of concepts like has_<member> that require providing the
   member call signature one of the following approaches may be used:

    * wrap the has_<member> into a more concrete concept:

      @code
      BOOST_TYPE_ERASURE_MEMBER ((has_get), get)

      template < class T =  _self >
      struct gettable:
      mpl::vector<
          has_get < void (), T > > {};

      using any_gettable_ptr = any_concept_ptr< gettable >;
      @endcode

    * use template alias:

      @code
      BOOST_TYPE_ERASURE_MEMBER ((has_get), get)

      template< ElementType >
      has_get_void = has_get < void (), ElementType >;

      using any_gettable_ptr = any_concept_ptr< has_get_void >;
      @endcode

   @tparam ConceptHead The head of the list of the concepts applied to the
   element type. The concept type must be a template tyee with a single
   parameter that is used to provide the type to which this concept is applied.

   @tparam ConceptTail The tail of the list of the concepts applied to the
   element type. Each concept type in the list must be a template type with a
   single parameter that is used to provide the type to which this concept is
   applied.
*/
template <
    template < typename ElementType > class ConceptHead,
    template < typename ElementType > class... ConceptTail >
struct any_concept_ptr
    : ::boost::mpl::vector<
          ::boost::type_erasure::relaxed, // To enable construction of an empty
                                          // object.
          ::boost::type_erasure::
              same_type< pointer<>::element_type, ::boost::type_erasure::_a >,
          pointer<>,
          _v1::detail::explicitly_convertible_to_bool<>,
          ConceptHead< ::boost::type_erasure::_a >,
          ConceptTail< ::boost::type_erasure::_a >... > {};


/**
   @brief A convenience macro to declare a type erasure concept for a pointer to
   an object of a type implementing the given concepts

   @details Declares a type erasure concept for a pointer to an object of a type
   implementing the given concepts with element_type nested type to access the
   any-type of the referenced value.

   @param [in] PTR_TYPE_NAME - the name of the declared type erasure pointer
   concept.

   @param [in] ... - a list of the concepts applied to the referenced type.
*/
#define UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR(PTR_TYPE_NAME, ...)              \
	struct PTR_TYPE_NAME                                                       \
	    : ::boost::mpl::vector<                                                \
	          ::utk::meta::type_erasure::any_concept_ptr< __VA_ARGS__ > > {    \
		using element_type = decltype (                                        \
		    *std::declval< ::boost::type_erasure::any< PTR_TYPE_NAME > > ());  \
	};


UTK_META_TYPE_ERASURE_V1_END_NAMESPACE


#endif /* UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR_HPP */
