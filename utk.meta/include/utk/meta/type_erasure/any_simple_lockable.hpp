// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.meta/utk.meta/include/utk/meta/type_erasure/any_simple_lockable.hpp
//
// Description: Declaration of type erasure types for working with BasicLockable
//              and Lockable objects.


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_ANY_SIMPLE_LOCKABLE_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_ANY_SIMPLE_LOCKABLE_HPP


#include <boost/mpl/vector.hpp>
#include <boost/type_erasure/any.hpp>

#include "utk/meta/type_erasure/detail/namespace.hpp"

#include "utk/meta/type_erasure/simple_lockable.hpp"
#include "utk/meta/type_erasure/simple_lockable_ptr.hpp"


UTK_META_TYPE_ERASURE_V1_BEGIN_NAMESPACE


/**
   @brief A type erasure type for BasicLockable objects

   @todo Move to utk:std
*/
using any_basic_lockable = ::boost::type_erasure::any< ::boost::mpl::vector<
    ::boost::type_erasure::constructible< ::boost::type_erasure::_self (
        ::boost::type_erasure::_self&&) >,
    ::boost::type_erasure::destructible<>,
    ::boost::type_erasure::relaxed,
    ::utk::meta::type_erasure::basic_lockable<> > >;


/**
   @brief A type erasure type for a pointer or a pointer-like object pointing to
   a BasicLockable object

   @todo Move to utk:std
*/
using any_basic_lockable_ptr = ::boost::type_erasure::any<
    ::boost::mpl::vector< ::utk::meta::type_erasure::basic_lockable_ptr > >;


/**
   @brief A type erasure type for a weak-pointer or a weak-pointer-like object
   pointing to a BasicLockable object

   @todo Move to utk:std
*/
using any_basic_lockable_weak_ptr =
    ::boost::type_erasure::any< ::boost::mpl::vector<
        ::utk::meta::type_erasure::basic_lockable_weak_ptr > >;


/**
   @brief A type erasure type for Lockable objects

   @todo Move to utk:std
*/
using any_lockable = ::boost::type_erasure::any< ::boost::mpl::vector<
    ::boost::type_erasure::constructible< ::boost::type_erasure::_self (
        ::boost::type_erasure::_self&&) >,
    ::boost::type_erasure::destructible<>,
    ::boost::type_erasure::relaxed,
    ::utk::meta::type_erasure::lockable<> > >;


/**
   @brief A type erasure type for a pointer or a pointer-like object pointing to
   a Lockable object

   @todo Move to utk:std
*/
using any_lockable_ptr = ::boost::type_erasure::any<
    ::boost::mpl::vector< ::utk::meta::type_erasure::lockable_ptr > >;


/**
   @brief A type erasure type for a weak-pointer or a weak-pointer-like object
   pointing to a Lockable object

   @todo Move to utk:std
*/
using any_lockable_weak_ptr = ::boost::type_erasure::any<
    ::boost::mpl::vector< ::utk::meta::type_erasure::lockable_weak_ptr > >;


UTK_META_TYPE_ERASURE_V1_END_NAMESPACE


#endif /* UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_ANY_SIMPLE_LOCKABLE_HPP */
