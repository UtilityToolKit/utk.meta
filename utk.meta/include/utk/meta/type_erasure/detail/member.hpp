// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.meta/utk.meta/include/utk/meta/type_erasure/detail/member.hpp
//
// Description: Provides convenience macro for declaring primitive concepts in
//              utk::meta::type_erasure namespace.


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_DETAIL_MEMBER_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_DETAIL_MEMBER_HPP


#include <boost/type_erasure/any.hpp>
#include <boost/type_erasure/member.hpp>

#include "utk/meta/type_erasure/detail/namespace.hpp"


#define UTK_META_TYPE_ERASURE_MEMBER(MEMBER_NAME)                              \
	UTK_META_TYPE_ERASURE_V1_BEGIN_NAMESPACE                                   \
	BOOST_TYPE_ERASURE_MEMBER ((has_##MEMBER_NAME), MEMBER_NAME)               \
	UTK_META_TYPE_ERASURE_V1_END_NAMESPACE


#endif /* UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_DETAIL_MEMBER_HPP */
