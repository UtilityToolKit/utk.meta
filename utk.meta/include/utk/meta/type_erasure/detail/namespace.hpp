// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.meta/utk.meta/include/utk/meta/type_erasure/detail/namespace.hpp
//
// Description: Declaration of the utk::meta::type_erasure::v1 namespace.


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_DETAIL_NAMESPACE_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_DETAIL_NAMESPACE_HPP


#define UTK_META_TYPE_ERASURE_V1_BEGIN_NAMESPACE                               \
	namespace utk {                                                            \
		namespace meta {                                                       \
			namespace type_erasure {                                           \
				inline namespace v1 {

#define UTK_META_TYPE_ERASURE_V1_END_NAMESPACE                                 \
	}                                                                          \
	}                                                                          \
	}                                                                          \
	}

#define UTK_META_TYPE_ERASURE_BASE_NAMESPACE_SEQUENCE                          \
	(utk) (meta) (type_erasure)

#define UTK_META_TYPE_ERASURE_VERSIONED_NAMESPACE_SEQUENCE                     \
	UTK_META_TYPE_ERASURE_BASE_NAMESPACE_SEQUENCE (v1)

#define UTK_META_TYPE_ERASURE_PRIVATE_VERSIONED_NAMESPACE_SEQUENCE             \
	UTK_META_TYPE_ERASURE_BASE_NAMESPACE_SEQUENCE (_v1) (detail)


#endif /* UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_DETAIL_NAMESPACE_HPP */
