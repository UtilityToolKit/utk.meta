// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.meta/utk.meta/include/utk/meta/type_erasure/members_of_lockable.hpp
//
// Description: Declaration of primitive concepts describing lockable types for
//              using with boost::type_erasure::any.


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_MEMBERS_OF_LOCKABLE_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_MEMBERS_OF_LOCKABLE_HPP


#include "utk/meta/type_erasure/detail/member.hpp"


/**
   @brief Primitive type erasure concepts for modeling BasicLockable
*/
UTK_META_TYPE_ERASURE_MEMBER (lock)

UTK_META_TYPE_ERASURE_MEMBER (unlock)


/**
   @brief Primitive type erasure concepts for modeling Lockable
*/
UTK_META_TYPE_ERASURE_MEMBER (try_lock)


/**
   @brief Primitive type erasure concepts for modeling TimedLockable
*/
UTK_META_TYPE_ERASURE_MEMBER (try_lock_until)

UTK_META_TYPE_ERASURE_MEMBER (try_lock_for)


/**
   @brief Primitive type erasure concepts for modeling SharableLockable
*/
UTK_META_TYPE_ERASURE_MEMBER (lock_shared)

UTK_META_TYPE_ERASURE_MEMBER (unlock_shared)

UTK_META_TYPE_ERASURE_MEMBER (try_lock_shared)

UTK_META_TYPE_ERASURE_MEMBER (try_lock_shared_until)

UTK_META_TYPE_ERASURE_MEMBER (try_lock_shared_for)


/**
   @brief Primitive type erasure concepts for modeling UpgradableLockable
*/
UTK_META_TYPE_ERASURE_MEMBER (lock_upgrade)

UTK_META_TYPE_ERASURE_MEMBER (unlock_upgrade)


UTK_META_TYPE_ERASURE_MEMBER (try_lock_upgrade)

UTK_META_TYPE_ERASURE_MEMBER (try_lock_upgrade_until)

UTK_META_TYPE_ERASURE_MEMBER (try_lock_upgrade_for)


UTK_META_TYPE_ERASURE_MEMBER (try_unlock_shared_and_lock)

UTK_META_TYPE_ERASURE_MEMBER (try_unlock_shared_and_lock_until)

UTK_META_TYPE_ERASURE_MEMBER (try_unlock_shared_and_lock_for)


UTK_META_TYPE_ERASURE_MEMBER (try_unlock_shared_and_lock_upgrade)

UTK_META_TYPE_ERASURE_MEMBER (try_unlock_shared_and_lock_upgrade_until)

UTK_META_TYPE_ERASURE_MEMBER (try_unlock_shared_and_lock_upgrade_for)


UTK_META_TYPE_ERASURE_MEMBER (try_unlock_upgrade_and_lock)

UTK_META_TYPE_ERASURE_MEMBER (try_unlock_upgrade_and_lock_until)

UTK_META_TYPE_ERASURE_MEMBER (try_unlock_upgrade_and_lock_for)

UTK_META_TYPE_ERASURE_MEMBER (try_unlock_upgrade_and_lock_shared)


UTK_META_TYPE_ERASURE_MEMBER (unlock_and_lock_shared)

UTK_META_TYPE_ERASURE_MEMBER (unlock_and_lock_upgrade)

UTK_META_TYPE_ERASURE_MEMBER (unlock_upgrade_and_lock)


#endif /* UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_MEMBERS_OF_LOCKABLE_HPP */
