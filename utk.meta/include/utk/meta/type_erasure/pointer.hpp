// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.meta/utk.meta/include/utk/meta/type_erasure/pointer.hpp
//
// Description: Declaration of type erasure concept pointer types for using with
//              "boost::type_erasure::any".


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_POINTER_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_POINTER_HPP


#include <boost/pointee.hpp>

#include <boost/mpl/vector.hpp>

#include <boost/type_erasure/any.hpp>
#include <boost/type_erasure/operators.hpp>

#include "utk/meta/type_erasure/detail/namespace.hpp"

#include "utk/meta/type_erasure/members_of_lockable.hpp"


UTK_META_TYPE_ERASURE_V1_BEGIN_NAMESPACE


namespace detail {
	template < class ElementType >
	struct pointee {
		typedef typename ::boost::mpl::eval_if<
		    ::boost::type_erasure::is_placeholder< ElementType >,
		    ::boost::mpl::identity< void >,
		    ::boost::pointee< ElementType > >::type type;
	};
} // namespace detail


/**
   @brief A type erasure concept representing any pointer or pointer-like type

   @note Based on
   https://www.boost.org/doc/libs/1_67_0/libs/type_erasure/example/associated.cpp
*/
template < class ElementType = ::boost::type_erasure::_self >
struct pointer
    : ::boost::mpl::vector<
          ::boost::type_erasure::copy_constructible< ElementType >,
          ::boost::type_erasure::dereferenceable<
              ::boost::type_erasure::deduced< detail::pointee< ElementType > >&,
              ElementType > > {
	typedef ::boost::type_erasure::deduced< detail::pointee< ElementType > >
	    element_type;
};


UTK_META_TYPE_ERASURE_V1_END_NAMESPACE


#endif /* UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_POINTER_HPP */
