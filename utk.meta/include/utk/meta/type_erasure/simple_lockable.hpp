// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.meta/utk.meta/include/utk/meta/type_erasure/simple_lockable.hpp
//
// Description: Declaration of type erasure concepts describing types
//              implementing STL and Boost BasicLockable and Lockable concepts
//              for using with "boost::type_erasure::any".


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_SIMPLE_LOCKABLE_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_SIMPLE_LOCKABLE_HPP


#include <boost/type_erasure/any.hpp>

#include "utk/meta/type_erasure/detail/namespace.hpp"

#include "utk/meta/type_erasure/members_of_lockable.hpp"


UTK_META_TYPE_ERASURE_V1_BEGIN_NAMESPACE

/**
   @brief Type erasure concept for types implementing BasicLockable concept

   @tparam Lockable - type of the stored instance of the basic lockable type.
*/
template < class Lockable = ::boost::type_erasure::_self >
struct basic_lockable : ::boost::mpl::vector<
                            has_lock< void(), Lockable >,
                            has_unlock< void(), Lockable > > {};


/**
   @brief Type erasure concept for types implementing Lockable concept

   @tparam Lockable - type of the stored instance of the lockable type.
*/
template < class Lockable = ::boost::type_erasure::_self >
struct lockable : ::boost::mpl::vector<
                      basic_lockable< Lockable >,
                      has_try_lock< bool(), Lockable > > {};


UTK_META_TYPE_ERASURE_V1_END_NAMESPACE


#endif // UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_SIMPLE_LOCKABLE_HPP
