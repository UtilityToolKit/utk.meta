// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.meta/utk.meta/include/utk/meta/type_erasure/simple_lockable_ptr.hpp
//
// Description: Declaration of type erasure concepts for pointers and weak
//              pointers to BasicLockable and Lockable objects for use with
//              "boost::type_erasure::any".


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_SIMPLE_LOCKABLE_PTR_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_SIMPLE_LOCKABLE_PTR_HPP


#include "utk/meta/type_erasure/detail/namespace.hpp"

#include "utk/meta/type_erasure/any_concept_ptr.hpp"
#include "utk/meta/type_erasure/any_concept_weak_ptr.hpp"
#include "utk/meta/type_erasure/simple_lockable.hpp"


UTK_META_TYPE_ERASURE_V1_BEGIN_NAMESPACE


/**
   @brief A type erasure concept a pointer or a pointer-like type to a
   BasicLockable object
*/
UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR (basic_lockable_ptr, basic_lockable)


/**
   @brief A type erasure concept a weak pointer or a weak-pointer-like type to a
   BasicLockable object
*/
UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (
    basic_lockable_weak_ptr, basic_lockable)


/**
   @brief A type erasure concept a pointer or a pointer-like type to a Lockable
   object
*/
UTK_META_TYPE_ERASURE_ANY_CONCEPT_PTR (lockable_ptr, lockable)


/**
   @brief A type erasure concept a weak pointer or a weak-pointer-like type to a
   Lockable object
*/
UTK_META_TYPE_ERASURE_ANY_CONCEPT_WEAK_PTR (lockable_weak_ptr, lockable)


UTK_META_TYPE_ERASURE_V1_END_NAMESPACE


#endif /* UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_SIMPLE_LOCKABLE_PTR_HPP */
