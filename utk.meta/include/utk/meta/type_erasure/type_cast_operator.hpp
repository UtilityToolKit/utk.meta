// Copyright 2018 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name:
// utk.meta/utk.meta/include/utk/meta/type_erasure/type_cast_operator.hpp
//
// Description: Macros to declare type erasure concepts for types that can be
//              cast to other types for using with "boost::type_erasure::any".


#ifndef UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_HPP
#define UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_HPP


#include <boost/type_erasure/detail/macro.hpp>

#include <boost/type_erasure/member.hpp>


/**
   @internal

   @brief Macro to generate type erasure concepts for type-cast operators

   @note Can only be used at global namespace scope with fully qualified concept
   type name. The limitations are the same as for the BOOST_TYPE_ERASURE_MEMBER
   macro in C++03.

   @details Generates type erasure concept with the name
   <EXPLICITY_PREFIX>_convertible_to_<TARGET_TYPE> that implements type-cast
   operator to type TARGET_TARGET with EXPLICIT_SPECIFIER explicity specifier.

   @param [in] TARGET_TYPE The qualified name of the type to cast to.

   @param [in] EXPLICITY_PREFIX The prefix added to the name of the concept to
   represent whether the type cast is explicit or implicit.

   @param [in] EXPLICIT_SPECIFIER Explicit specifier to add to the cast operator
   declaration. Should be "exlicit" or empty for implicit type-cast operator.
*/
#define UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_II(                           \
    QUALIFIED_NAME, CONCEPT_NAME, TARGET_TYPE, EXPLICIT_SPECIFIER)             \
	BOOST_TYPE_ERASURE_OPEN_NAMESPACE (QUALIFIED_NAME)                         \
	template < class AnyType = ::boost::type_erasure::_self >                  \
	struct CONCEPT_NAME {                                                      \
		static TARGET_TYPE apply (const AnyType& arg) {                        \
			return TARGET_TYPE (arg);                                          \
		}                                                                      \
	};                                                                         \
	BOOST_TYPE_ERASURE_CLOSE_NAMESPACE (QUALIFIED_NAME)                        \
                                                                               \
	namespace boost {                                                          \
		namespace type_erasure {                                               \
			template < class AnyType, class Base >                             \
			struct concept_interface<                                          \
			    BOOST_TYPE_ERASURE_QUALIFIED_NAME (QUALIFIED_NAME) <           \
			    AnyType >,                                                     \
			    Base, AnyType,                                                 \
			    typename ::boost::enable_if<                                   \
			        ::boost::type_erasure::detail::                            \
			            should_be_non_const< AnyType, Base > >::type >         \
			    : Base {                                                       \
				EXPLICIT_SPECIFIER operator TARGET_TYPE () {                   \
					return ::boost::type_erasure::call (                       \
					    BOOST_TYPE_ERASURE_QUALIFIED_NAME (QUALIFIED_NAME) <   \
					        AnyType > (),                                      \
					    *this);                                                \
				}                                                              \
			};                                                                 \
                                                                               \
                                                                               \
			template < class AnyType, class Base >                             \
			struct concept_interface<                                          \
			    BOOST_TYPE_ERASURE_QUALIFIED_NAME (QUALIFIED_NAME) <           \
			    AnyType >,                                                     \
			    Base, AnyType,                                                 \
			    typename ::boost::enable_if<                                   \
			        ::boost::type_erasure::detail::                            \
			            should_be_const< AnyType, Base > >::type > : Base {    \
				EXPLICIT_SPECIFIER operator TARGET_TYPE () const {             \
					return ::boost::type_erasure::call (                       \
					    BOOST_TYPE_ERASURE_QUALIFIED_NAME (QUALIFIED_NAME) <   \
					        AnyType > (),                                      \
					    *this);                                                \
				}                                                              \
			};                                                                 \
		}                                                                      \
	}


/**
   @internal

   @brief Intermediate macro to generate neccessery inputs for the main macro
*/
#define UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_I(                            \
    TARGET_TYPE, EXPLICITY_PREFIX, EXPLICIT_SPECIFIER, NAMESPACE)              \
	UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_II (                              \
	    NAMESPACE (EXPLICITY_PREFIX##_convertible_to_##TARGET_TYPE),           \
	    BOOST_PP_SEQ_ELEM (                                                    \
	        BOOST_PP_DEC (BOOST_PP_SEQ_SIZE (                                  \
	            NAMESPACE (EXPLICITY_PREFIX##_convertible_to_##TARGET_TYPE))), \
	        NAMESPACE (EXPLICITY_PREFIX##_convertible_to_##TARGET_TYPE)),      \
	    TARGET_TYPE,                                                           \
	    EXPLICIT_SPECIFIER)


/**
   @brief Macro to generate type erasure concepts for explicit type-cast
   operators

   @note Can only be used at global namespace scope. The limitations are the
   same as for the BOOST_TYPE_ERASURE_MEMBER macro in C++03.

   @details Generates type erasure concept at global scope with the name
   explicitly_convertible_to_<TARGET_TYPE> that implements explicit type-cast
   operator to type TARGET_TARGET.

   @param [in] TARGET_TYPE The name of the type to implicitly cast to.
*/
#define UTK_META_TYPE_ERASURE_EXPLICITLY_CONVERTIBLE_TO(TARGET_TYPE)           \
	UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_I (                               \
	    TARGET_TYPE, explicitly, explicit, /* global namespace */)


/**
   @brief Macro to generate type erasure concepts for explicit type-cast
   operators

   @note Can only be used at global namespace scope with fully qualified concept
   type name. The limitations are the same as for the BOOST_TYPE_ERASURE_MEMBER
   macro in C++03.

   @details Generates type erasure concept at the given namespace with the name
   explicitly_convertible_to_<TARGET_TYPE> that implements explicit type-cast
   operator to type TARGET_TARGET. The namespace should not be inline to prevent
   warnings from the compiler.

   @param [in] TARGET_TYPE The name of the type to implicitly cast
   to.

   @param [in] NAMESPACE The Boost Preprocessor sequence representing the
   namespace to put the generated concept declaration at,
   e.g. (boost)(type_erasure) for boost::type_erasure namespace.
*/
#define UTK_META_TYPE_ERASURE_EXPLICITLY_CONVERTIBLE_TO_QUALIFIED(             \
    TARGET_TYPE, NAMESPACE)                                                    \
	UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_I (                               \
	    TARGET_TYPE, explicitly, explicit, NAMESPACE)


/**
   @brief Macro to generate type erasure concepts for implicit type-cast
   operators

   @note Can only be used at global namespace scope. The limitations are the
   same as for the BOOST_TYPE_ERASURE_MEMBER macro in C++03.

   @details Generates type erasure concept at global scope with the name
   implicitly_convertible_to_<TARGET_TYPE> that implements implicit type-cast
   operator to type TARGET_TARGET.

   @param [in] TARGET_TYPE The name of the type to implicitly cast to.
*/
#define UTK_META_TYPE_ERASURE_IMPLICITLY_CONVERTIBLE_TO(TARGET_TYPE)           \
	UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_I (                               \
	    TARGET_TYPE, implicitly, /* implicit */, /* global namespace */)


/**
   @brief Macro to generate type erasure concepts for implicit type-cast
   operators

   @note Can only be used at global namespace scope with fully qualified concept
   type name. The limitations are the same as for the BOOST_TYPE_ERASURE_MEMBER
   macro in C++03.

   @details Generates type erasure concept at the given namespace with the name
   implicitly_convertible_to_<TARGET_TYPE> that implements implicit type-cast
   operator to type TARGET_TARGET. The namespace should not be inline to prevent
   warnings from the compiler.

   @param [in] TARGET_TYPE The name of the type to implicitly cast
   to.

   @param [in] NAMESPACE The Boost Preprocessor sequence representing the
   namespace to put the generated concept declaration at,
   e.g. (boost)(type_erasure) for boost::type_erasure namespace.
*/
#define UTK_META_TYPE_ERASURE_IMPLICITLY_CONVERTIBLE_TO_QUALIFIED(             \
    TARGET_TYPE, NAMESPACE)                                                    \
	UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_I (                               \
	    TARGET_TYPE, implicitly, /* implicit */, NAMESPACE)


#endif /* UTK_META_INCLUDE_UTK_META_TYPE_ERASURE_TYPE_CAST_OPERATOR_HPP */
